# Package

version       = "1.2.0"
author        = "Anna Aurora"
description   = "A simple implementation of the classic snake game"
license       = "LGPL-3.0-only"
srcDir        = "src"
bin           = @["snekim"]


# Dependencies

requires "nim >= 1.6.6"
requires "nimraylib_now"

