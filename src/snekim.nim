# This file is part of the snekim source code.
#
# ©️ 2022 Anna Aurora Neugebauer <mailto:anna@annaaurora.eu> <https://matrix.to/#/@papojari:artemislena.eu> <https://annaaurora.eu>
#
# For the license information, please view the README.md file that was distributed with this source code.

import nimraylib_now, std/[random, strformat, os, json, jsonutils]

type
  EntityColors = object
    border: Color
    content: Color

type
  Text = object
    size: Natural
    color: Color

type
  Theme = object
    blockLength: Natural
    groundColors: EntityColors
    text: Text

type Location = object
  x: int
  y: int

type
  Entity = ref object of RootObj
    colors: EntityColors
    direction: string
    inputDirection: string
    location: Location
    locationChange: Location
    locationHistory: seq[Location]

type
  Snake = ref object of Entity
    length: Natural

type
  Screen = object
    width: Natural
    height: Natural

type
  Game = object
    appleEaten: bool
    over: bool
    paused: bool
    controls: string
    controlsShow: bool

type
  Timer = object
    startTime: float
    lifeTime: float

type
  UserData = object
    highScore: Natural

proc findMin(x: int, y: int): int =  
  if x < y:
    return x  
  else:
    return y

proc toggle(bool: bool): bool =
  if bool:
    return false
  else:
    return true

proc timerDone(timer: Timer): bool =
  return getTime() - timer.startTime >= timer.lifeTime

proc drawTextCentered(text: string, textSize: Natural, textColor: Color, screenWidth: Natural, screenHeight: Natural) =
  var x = ((screenWidth / 2).int - (measureText(text, textSize.int) / 2).int)
  var y = (screenHeight / 2).int

  drawText(text, x, y, textSize.int, textColor)

when isMainModule:
  const theme = Theme(
    blockLength: 8,
    groundColors: EntityColors(
      border: (40, 100, 40, 255),
      content: (40, 127, 40, 255)
    ),
    text: Text(
      size: 1,
      color: (158, 255, 0, 127)
    )
  )

  const screen = Screen(
    width: theme.blockLength * 32,
    height: theme.blockLength * 32
  )

  var game = Game(
    appleEaten: false,
    over: false,
    paused: true,
    controls: "",
    controlsShow: true
  )

  game.controls.add("\nGame controls:")
  game.controls.add("\n	- W, A, S, D to move snake")
  game.controls.add("\n	- R to restart or start")
  game.controls.add("\n	- P, SPACE to toggle pause")
  game.controls.add("\n	- H to toggle game controls overlay")

  var camera = Camera2D(
    target: (0.0, 0.0),
    offset: (0.0, 0.0),
    rotation: 0.0,
    zoom: 1.0,
  )

  var timer = Timer(
    startTime: getTime(),
    lifeTime: 0.1
  )

  var apple = Entity(
    colors: EntityColors(
      border: (145, 0, 30, 255),
      content: (255, 0, 0, 255)
    ),
    location: Location(
      x: screen.width + theme.blockLength,
      y: screen.height + theme.blockLength
    )
  )

  var snake = Snake(
    colors: EntityColors(
      border: (255, 45, 0, 255),
      content: (255, 127, 0, 255)
    ),
    direction: "RIGHT",
    inputDirection: "RIGHT",
    location: Location(
      x: (screen.width / 2).int,
      y: (screen.height / 2).int
    ),
    locationChange: Location(
      x: theme.blockLength,
      y: 0
    ),
    locationHistory: @[],
    length: 1
  )

  var score: Natural = 0

  apple.location.x = rand((screen.width - theme.blockLength) / theme.blockLength).int * theme.blockLength
  apple.location.y = rand((screen.width - theme.blockLength) / theme.blockLength).int * theme.blockLength

  snake.locationHistory.add(snake.location)

  setConfigFlags(Window_Resizable or Vsync_Hint)
  initWindow(screen.width.int,screen.height.int, "snekim")

  var userData = UserData(
    highScore: 0
  )

  let userDataDir = fmt"{getConfigDir()}snekim"
  let userDataPath = fmt"{userDataDir}/user-data.json"

  proc makeSureUserDataDirExists() =
    if dirExists(userDataDir) == false:
      createDir(userDataDir)

  proc saveUserData() =
    makeSureUserDataDirExists()
    writeFile(userDataPath, fmt"{userData.toJson}")

  proc createUserData() =
    if fileExists(userDataPath):
      # Read userData.json file, parse it into a Nim json object, then parse it into a Nim object.
      userData = to(parseJson(readFile(userDataPath)), UserData)
    else:
      saveUserData()

  createUserData()

  while not windowShouldClose():
    var shortestScreenLength = findMin(getScreenWidth(), getScreenHeight())
    var scalingFactor = (shortestScreenLength / screen.width)

    camera.zoom = 1 * scalingFactor

    beginDrawing()

    clearBackground(theme.groundColors.border)

    beginMode2D camera

    drawRectangle(0, 0, screen.width.int, screen.height.int, theme.groundColors.content)

    if isKeyPressed(R):
      snake.direction = "RIGHT"
      snake.inputDirection = "RIGHT"
      snake.location.x = (screen.width / 2).int
      snake.location.y = (screen.height / 2).int
      snake.locationChange.x = theme.blockLength
      snake.locationHistory = @[]
      snake.locationHistory.add(snake.location)
      snake.length = 1
      score = 0
      timer.lifetime = 0.1

      game.over = false

    if isKeyPressed(P) or isKeyPressed(SPACE):
      game.paused = toggle(game.paused)

    if isKeyPressed(H):
      game.controlsShow = toggle(game.controlsShow)

    if not game.over and not game.paused and timerDone(timer):
      # Map keys to input direction.
      if isKeyDown(A):
        snake.inputDirection = "LEFT"
      elif isKeyDown(D):
        snake.inputDirection = "RIGHT"
      elif isKeyDown(W):
        snake.inputDirection = "UP"
      elif isKeyDown(S):
        snake.inputDirection = "DOWN"

      # Make sure that the snake can't go in the opposite direction it's currently going.
      case snake.inputDirection:
        of "UP":
          if snake.direction != "DOWN":
            snake.direction = "UP"
        of "DOWN":
          if snake.direction != "UP":
            snake.direction = "DOWN"
        of "LEFT":
          if snake.direction != "RIGHT":
            snake.direction = "LEFT"
        of "RIGHT":
          if snake.direction != "LEFT":
            snake.direction = "RIGHT"

      # Map direction to location change.
      case snake.direction:
        of "LEFT":
          snake.locationChange.x = -theme.blockLength
          snake.locationChange.y = 0
        of "RIGHT":
          snake.locationChange.x = theme.blockLength
          snake.locationChange.y = 0
        of "UP":
          snake.locationChange.x = 0
          snake.locationChange.y = -theme.blockLength
        of "DOWN":
          snake.locationChange.x = 0
          snake.locationChange.y = theme.blockLength

      if snake.location.x >= screen.width or snake.location.x < 0 or snake.location.y >= screen.height or snake.location.y < 0:
        game.over = true

      snake.location.x += snake.locationChange.x
      snake.location.y += snake.locationChange.y

      if game.appleEaten:
        apple.location.x = rand((screen.width - theme.blockLength) / theme.blockLength).int * theme.blockLength
        apple.location.y = rand((screen.width - theme.blockLength) / theme.blockLength).int * theme.blockLength

        snake.length += 1
        score = snake.length - 1
        timer.lifetime -= 0.1 / 40

        if score > userData.highScore:
          userData.highScore = score
          saveUserData()

        game.appleEaten = false

      if snake.location.x == apple.location.x and snake.location.y == apple.location.y:
        game.appleEaten = true

      # Lose if snake stepped onto itself.
      for x in snake.locationHistory:
        if x == snake.location:
          game.over = true

      snake.locationHistory.add(snake.location)

      # Don't let `snake.locationHistory` exceed more than `snake.length` items.
      if snake.locationHistory.len > snake.length:
        snake.locationHistory.delete(0)

      timer.startTime = getTime()

    drawRectangle(apple.location.x.cint, apple.location.y.cint, theme.blockLength.cint, theme.blockLength.cint, apple.colors.content)
    drawRectangleLines(apple.location.x.cint, apple.location.y.cint, theme.blockLength.cint, theme.blockLength.cint, apple.colors.border)

    # Draw snake.
    for i in snake.locationHistory:
      drawRectangle(i.x.cint, i.y.cint, theme.blockLength.cint, theme.blockLength.cint, snake.colors.content)
      drawRectangleLines(i.x.cint, i.y.cint, theme.blockLength.cint, theme.blockLength.cint, snake.colors.border)

    if game.over:
      drawTextCentered("You lost.", theme.text.size, theme.text.color, screen.width, screen.height)

    var text = ""

    text.add(fmt"{getFPS()} FPS")
    text.add("\n")
    text.add(fmt"High score: {userData.highScore}")
    text.add("\n")
    text.add(fmt"Score: {score}")

    if game.controlsShow:
      text.add(game.controls)

    drawText(text, 1, 1, theme.text.size.int, theme.text.color)

    endMode2D()

    endDrawing()

