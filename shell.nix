with (import <nixpkgs> { config.allowUnfree = true; });

mkShell {
  buildInputs = [
    # Dependencies
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXinerama
    xorg.libXi
    xorg.libXext

    # Nim's package manager for compiling the project.
    nimble-unwrapped

    # For running the binary.
    steam-run
  ];
}
