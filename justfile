#!/usr/bin/env just --justfile
name:='snekim'
build-path:='./build'

# By default, recipes are only listed.
default:
	@just --list

build-icons:
	#!/bin/sh
	set -euxo pipefail
	# Clear {{build-path}}
	mkdir -p {{build-path}}
	rm -rf {{build-path}}
	# .desktop
	install -Dm600 {{name}}.desktop -t {{build-path}}/usr/share/applications
	# Icons
	mkdir -p {{build-path}}/usr/share/icons/hicolor
	for icon_width in 48 128; do
		mkdir -p {{build-path}}/usr/share/icons/hicolor/$icon_width"x"$icon_width/apps
		inkscape -o {{build-path}}/usr/share/icons/hicolor/$icon_width"x"$icon_width/apps/{{name}}.png -C -w $icon_width -h $icon_width --export-png-color-mode=RGBA_8 icons/hicolor/48x48/{{name}}.svg
	done

