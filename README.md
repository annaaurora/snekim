# snekim 🍎🐍

![snekim showcase screenshot](screenshot.png)

A simple implementation of the classic snake game.

## Reason for making

I made this game to try out [raylib](https://www.raylib.com/).

## Installation

### Prerequisites

snekim runs on Linux. I have no idea if you can get it to work with nimble on Windows or macOS. If you do, [contact me](https://annaaurora.eu/accounts#contact).

### Packages

| Operating System                                     | Package Manager  | Package                      | Command                                                                       |
| ---------------------------------------------------- | ---------------- | ---------------------------- | ----------------------------------------------------------------------------- |
| [Various][nim-platforms]                             | [Nimble][nimble] | [snekim][snekim-nimble-pkg]  | `nimble install snekim`                                                        |
| [Arch Linux][arch linux]                             | [pacman][pacman] | [snekim][snekim-pacman]      | `git clone https://aur.archlinux.org/snekim.git && cd snekim && makepkg -sri` |
| [NixOS][nixos], [Linux][nix-plat], [macOS][nix-plat] | [Nix][nix]       | [snekim][snekim-nixpkg]      | `nix-env -iA nixos.snekim` or `nix-env -iA nixpkgs.snekim`                    |

[nim-platforms]: https://nim-lang.org/features.html
[nimble]: https://github.com/nim-lang/nimble#readme
[snekim-nimble-pkg]: https://codeberg.org/annaaurora/snekim
[arch linux]: https://www.archlinux.org
[pacman]: https://wiki.archlinux.org/title/Pacman
[snekim-pacman]: https://aur.archlinux.org/packages/snekim
[nixos]: https://nixos.org/nixos/
[nix-plat]: https://nixos.org/nix/manual/#ch-supported-platforms
[nix]: https://nixos.org/nix/
[snekim-nixpkg]: https://github.com/NixOS/nixpkgs/blob/master/pkgs/games/snekim/default.nix

![package version table](https://repology.org/badge/vertical-allrepos/snekim.svg)

## Running
 
See [the installation section](#installation) for how to install snekim on your computer.

Run snekim with `snekim` or via your launcher.

## License

### Code

©️ 2022 Anna Aurora Neugebauer <mailto:anna@annaaurora.eu> <https://matrix.to/#/@papojari:artemislena.eu> <https://annaaurora.eu>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License version 3 along with this program. If not, see <https://www.gnu.org/licenses/>.

### [`icons/hicolor/48x48/snekim.svg`](icons/hicolor/48x48/snekim.svg), [`screenshot.png`](screenshot.png)

©️ 2022 Anna Aurora Neugebauer. This work is licensed under the CC0 license.
